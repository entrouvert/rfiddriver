import socket

from datetime import datetime
from time import sleep


def unix_time(dt):
    epoch = datetime.utcfromtimestamp(0)
    delta = dt - epoch
    return delta.total_seconds()


def netcat(hostname, port, content):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((hostname, port))
    s.sendall(content)
    sleep(0.005)
    s.shutdown(socket.SHUT_WR)
    s.close()

#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
    Copyright (C) 2016 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


import os
import json
import logging


DEFAULT_CONFIG = {
    # Reader and transmitter general configuration
    'temperature': True,                # Analyse and record temperature from reading
    'heartrate': True,                  # Analyse and record heartrate from reading

    # Reader general configuration
    'reader_sleep_short': 0.1,          # Sleep between inventory of detection
    'reader_sleep_long': 5,             # Sleep before a long inventory

    # Flags to run veadista reading
    'exit_reader': False,               # Exit reading function
    'pause_reader': False,              # Suspend reading

    # Info produced by reading
    'reader_ready': False,              # Flag set when the RFID reader is ready
    'antenna_connected': [],            # List antennas connected
    'tag_in_field': '',                 # The last antenna the tag was detected

    # Transmitter general configuration
    'carbon_hostname': '127.0.0.1',     # carbon hostname
    'carbon_port': 2003,                # carbon post
    'metrics_path': 'entrouvert.research.veadista.patients.monitoring',  # whisper root path
    'patient_id': None,                 # patient id
    'transmitter_sleep': 1,             # Waiting time between transmissions

    # Flags to act during veadista sending
    'exit_transmitter': False,          # Exit reading function
    'pause_transmitter': False,         # Suspend transmission
}


logger = logging.getLogger(__name__)


class Config:
    def __init__(self):
        if not os.path.isfile('config.json'):
            logger.debug("Create configuration file '{}'.".format(DEFAULT_CONFIG))
            self.set_config(DEFAULT_CONFIG)
        else:
            logger.debug('Existing configuration file.')
            try:
                with open('config.json', 'r') as f:
                    json.loads(f.read())
            except ValueError:
                logger.warning("Corrupted configuration file, reinit with default configuration '{}'.".format(DEFAULT_CONFIG))
                self.set_config(DEFAULT_CONFIG)

    def set_config(self, config):
        with open('config.json', 'w') as f:
            f.write(json.dumps(config, sort_keys=True,
                    indent=4, separators=(',', ': ')))

    def get_config(self):
        with open('config.json', 'r') as f:
            return json.loads(f.read())

    def set_setting(self, key, value):
        config = self.get_config()
        config[key] = value
        self.set_config(config)

    def get_setting(self, key):
        config = self.get_config()
        return config.get(key)

    def reader_config(self):
        config = self.get_config()
        return (config.get('exit_reader'),
                config.get('pause_reader'),
                config.get('reader_sleep_short'),
                config.get('temperature'),
                config.get('heartrate'))

    def transmitter_config(self):
        config = self.get_config()
        return (config.get('exit_transmitter'),
                config.get('pause_transmitter'),
                config.get('transmitter_sleep'),
                config.get('patient_id'),
                config.get('carbon_hostname'),
                config.get('carbon_port'),
                config.get('temperature'),
                config.get('heartrate'),
                config.get('metrics_path'))

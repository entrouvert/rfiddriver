#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
    Copyright (C) 2016 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    E.g. with run with:
    python -m manager.server -b 127.0.0.1:9000

    Get config with:
    curl --header "Accept: application/json" http://127.0.0.1:9000/config

    Get one setting with:
    curl --header "Accept: application/json" http://127.0.0.1:9000/your_setting

    Set config with:
    curl -H "Content-Type: application/json" -X POST -d 'your_json_config' \
        --header "Accept: application/json" http://127.0.0.1:9000/config

    Set setting with:
    curl -H "Content-Type: application/json" -X POST -d '{"your_setting": value}' \
        --header "Accept: application/json" http://127.0.0.1:9000/your_setting
'''


import sys
import os

from gunicorn.app.wsgiapp import WSGIApplication


class WSGIApplication(WSGIApplication):

    def init(self, parser, opts, args):
        self.cfg.set("default_proc_name", "manager.wsgi:application")
        self.app_uri = "manager.wsgi:application"

        sys.path.insert(0, os.getcwd())


def main():
    WSGIApplication("%prog [OPTIONS]").run()


if __name__ == "__main__":
    main()

#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
    Copyright (C) 2016 Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''


import sys
import getopt

from time import sleep

import rfiddriver


def main(argv):
    def _exit():
        print 'reader-ctl.py -c command [-sxyzl -a logic_port -d delay -p power -S sleep_time]'
        sys.exit()
    path = ''
    command = ''
    logic_port = None
    delay = None
    power = None
    silent = False
    loop = False
    epcs_csv = False
    metrics_csv = False
    metrics_db = False
    slip = 0
    try:
        opts, args = getopt.getopt(argv, "hsxyzlc:a:d:p:S:",
                                   ["command=", "logic_port=", "delay=", "power=", "sleep="])
    except getopt.GetoptError:
        _exit()
    if not opts:
        _exit()
    for opt, arg in opts:
        if opt == '-h':
            _exit()
        if opt == '-s':
            silent = True
        if opt == '-l':
            loop = True
        if opt == '-x':
            epcs_csv = True
        if opt == '-y':
            metrics_csv = True
        if opt == '-z':
            metrics_db = True
        if opt in ("-c", "--command"):
            command = arg
        if opt in ("-a", "--logic_port"):
            try:
                logic_port = int(arg)
            except:
                _exit()
        if opt in ("-d", "--delay"):
            try:
                delay = int(arg)
            except:
                _exit()
        if opt in ("-p", "--power"):
            try:
                power = int(arg)
            except:
                _exit()
        if opt in ("-S", "--sleep"):
            try:
                slip = float(arg)
            except:
                _exit()
    if not command:
        _exit()
    reader = rfiddriver.Reader()
    if not power:
        power = 31
    if not delay:
        delay = 100
    if command == 'read':
        reader.read(power=power, logic_port=logic_port, silent=silent,
                    epcs_csv=epcs_csv, metrics_csv=metrics_csv, metrics_db=metrics_db)
    elif command == 'inventory':
        reader.inventory(delay=delay, power=power, logic_port=logic_port, silent=silent,
                         epcs_csv=epcs_csv, metrics_csv=metrics_csv, metrics_db=metrics_db)
        if loop:
            while True:
                sleep(slip)
                reader.inventory(delay=delay, power=power, logic_port=logic_port, silent=silent,
                                 epcs_csv=epcs_csv, metrics_csv=metrics_csv, metrics_db=metrics_db)
    elif command == 'detection':
        reader.detection(delay=delay, power=power, logic_port=logic_port, silent=silent)
        if loop:
            while True:
                sleep(slip)
                reader.detection(delay=delay, power=power, logic_port=logic_port, silent=silent)
    elif command == 'check':
        reader.device_check()
    elif command == 'send':
        transmitter = rfiddriver.Transmitter()
        transmitter.send()
    else:
        _exit()

if __name__ == "__main__":
    main(sys.argv[1:])
